<?php

require("config.php");
require_once("routing.php");

if (file_exists(VIEW . "/code/$view.php")) {
	require(VIEW . "/code/_base.php");
	require(VIEW . "/code/$view.php");
	if (class_exists("$view")) {
		$class = new ReflectionClass("$view");
		$m = $class->newInstance($notifications, $log);
		if ($isPost) {
			$postMethod = $class->getMethod("post");
			$postMethod->invoke($m);
		}
		$getMethod = $class->getMethod("get");
		$getMethod->invoke($m);
	}
}
require(VIEW . "/html/common/header.php");
require(VIEW . "/html/$view.php");
require(VIEW . "/html/common/footer.php");
