<?php

require("config.php");
require(VIEW . "/html/common/header.php");

if ($_SERVER['QUERY_STRING'] == "404") require(VIEW . "/html/common/404.php");
elseif ($_SERVER['QUERY_STRING'] == "500") require(VIEW . "/html/common/500.php");

require(VIEW . "/html/common/footer.php");
