<form method="POST">
    <fieldset class="c">
        <table>
            <tr>
                <td><label for="username">Username:</label></td>
                <td><input type="text" id="username" name="username" required /></td>
            </tr>
            <tr>
                <td><label for="password">Password:</label></td>
                <td><input type="password" id="password" name="password" required /></td>
            </tr>
            <tr>
                <td></td>
                <td><button type="submit" class="r p">Login</button></td>
            </tr>
        </table>
    </fieldset>
</form>