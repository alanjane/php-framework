<?php

class Auth
{
    private $authActive;
    private $isPost;
    private $notification;
    private $log;
    private $roles = [];

    function __construct($authActive, $isPost, $notification, $log, $roles)
    {
        $this->authActive = $authActive;
        $this->isPost = $isPost;
        $this->notification = $notification;
        $this->log = $log;
        $this->roles = $roles;
    }

    // Perform authentication.
    public function auth()
    {
        if (!$this->authActive) header("Location: /");

        session_start();
        session_regenerate_id();
        if (!$this->isLoggedin()) {
            header("Location: /login");
            die();
        } else {
            return R::findOne("user", "username = ?", [$_SESSION["username"]]);
        }
    }

    // Check if user is logged in.
    public function isLoggedin()
    {
        if (!$this->authActive) header("Location: /");

        return isset($_SESSION["username"]);
    }

    // Registration.
    public function register()
    {
        if (!$this->authActive) header("Location: /");

        global $PROD;
        session_start();
        if ($this->isPost) {
            // Validations.
            $validationsPassed = true;
            // Validate required fields.
            if ($_POST["username"] === "") {
                $this->notification->addMessage("Username is required.", Notification::ERROR);
                $validationsPassed = false;
            }
            if (strlen($_POST["username"]) > 15) {
                $this->notification->addMessage("Maximum length of username is 15 characters.", Notification::ERROR);
                $validationsPassed = false;
            }
            if ($_POST["password"] === "") {
                $this->notification->addMessage("Password is required.", Notification::ERROR);
                $validationsPassed = false;
            }
            if ($_POST["password2"] === "") {
                $this->notification->addMessage("Repeated password is required.", Notification::ERROR);
                $validationsPassed = false;
            }
            // Validate if username already exists.
            $existingUsername = R::findOne("user", "username = ?", [$_POST["username"]]);
            if ($existingUsername !== null) {
                $this->notification->addMessage("Username is already taken.", Notification::ERROR);
                $validationsPassed = false;
            }
            // Validate if passwords match.
            if (trim($_POST["password"]) !== trim($_POST["password2"])) {
                $this->notification->addMessage("Passwords do not match.", Notification::ERROR);
                $validationsPassed = false;
            }
            // Validate password length.
            if (strlen(trim($_POST["password"])) < 15) {
                $this->notification->addMessage("Password has to be at least 15 characters long.", Notification::ERROR);
                $validationsPassed = false;
            }
            // Validate invite code.
            if (trim($_POST["inviteCode"]) !== "oczq7XKr01QZ6ri9HZy8oH4WNiowtkntYzFxgIq6") {
                $this->notification->addMessage("Wrong invite code.", Notification::ERROR);
                $validationsPassed = false;
                if ($PROD)
                    sleep(60);
            }

            if ($validationsPassed) {
                // Add new user.
                R::begin();
                try {
                    $user = R::dispense("user");
                    $role = R::findOne("role", "name like ?", [(R::count("user") == 0 ? $this->roles[0] : $this->roles[1])]);
                    if ($role == NULL) $role = R::dispense("role");
                    $role->name = (R::count("user") == 0 ?  $this->roles[0] : $this->roles[1]);
                    $user->username = $_POST["username"];
                    $user->password = password_hash($_POST["password"], PASSWORD_DEFAULT);
                    $user->sharedTagList[] = $role;
                    R::store($user);
                    R::commit();
                    $this->login();
                } catch (Exception $e) {
                    R::rollback();
                    $this->notification->addMessage("Something went wrong while adding new user.", Notification::ERROR);
                }
            }
        }

        $this->notification->display();
        include_once(APP . "/auth/html/register.php");
    }

    // Login.
    public function login()
    {
        if (!$this->authActive) header("Location: /");

        global $PROD;
        session_start();
        if ($this->isPost) {
            // Validations.
            $validationsPassed = true;
            // Validate required fields.
            if ($_POST["username"] === "") {
                $this->notification->addMessage("Username is required.", Notification::ERROR);
                $validationsPassed = false;
            }
            if ($_POST["password"] === "") {
                $this->notification->addMessage("Password is required.", Notification::ERROR);
                $validationsPassed = false;
            }

            // Sleep to counter bruce force attack.
            if ($PROD)
                sleep(10);

            if ($validationsPassed) {
                // Find user.
                try {
                    $user = R::findOne("user", "username = ?", [$_POST["username"]]);
                    // Check if user exists and password matches.
                    if (
                        $user != null &&
                        $_POST["username"] === $user->username &&
                        password_verify($_POST["password"], $user->password)
                    ) {
                        $_SESSION["username"] = $user->username;
                        $_SESSION["userId"] = $user->id;
                        header("Location: /");
                        die();
                    } else {
                        $this->notification->addMessage("User with such username and password combination is not found.", Notification::ERROR);
                    }
                } catch (Exception $e) {
                    $this->notification->addMessage("Something went wrong while logging in user.", Notification::ERROR);
                }
            }
        }

        $this->notification->display();
        include_once(APP . "/auth/html/login.php");
    }

    // Logout.
    public function logout()
    {
        if (!$this->authActive) header("Location: /");

        session_start();
        unset($_SESSION["username"]);
        unset($_SESSION["userId"]);
        session_destroy();
        session_write_close();
        header("Location: /login");
        die();
    }

    public function getUsers()
    {
        $users = R::findAll("user");
        foreach ($users as $user) {
            $user->roles = [];
            $roles = R::getAll("
                select      r.name
                from        role as r
                left join   role_user as ru
                on          r.id = ru.role_id
                where       ru.user_id = :user_id
            ", ["user_id" => $user->id]);
            foreach ($roles as $role) {
                array_push($user->roles, $role);
            }
        }
        return $users;
    }
}
