<?php

class roles
{
    public const admin = "admin";
    public const user = "user";
    private $authActive;
    private $user = null;
    public $roles = [];

    public function __construct($authActive)
    {
        array_push($this->roles, roles::admin);
        array_push($this->roles, roles::user);
        $this->authActive = $authActive;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function hasRole($role)
    {
        if (!$this->authActive || $this->user === null) return false;
        $roleId = R::findOne("role", "where name = ?", [$role])->id;
        $userRole = R::findOne(
            "role_user",
            "where user_id = :user_id and role_id = :role_id",
            [
                "user_id" => $this->user->id,
                "role_id" => $roleId
            ]
        );
        if ($userRole == null) return false;
        return true;
    }
}
