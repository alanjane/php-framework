<?php
class log
{
	private $logPath;

	public function __construct($logPath)
	{
		$this->logPath = $logPath;
		if (!is_dir($this->logPath)) {
			mkdir($this->logPath);
		}
	}

	public function write($message)
	{
		$filename = date("Y_m_d");
		$time = date("H:i:s");
		try {
			$file = fopen("$this->logPath/$filename", "a");
			fwrite($file, "$time\n$message\n\n");
			fclose($file);
		} catch (Exception $e) {
			// Don't break app if logger don't work.
		}
	}
}
