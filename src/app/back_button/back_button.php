<?php

function backButton($buttonText = "Back", $classes = "")
{
    echo "<a class='" . $classes . "' href=" . $_SERVER['HTTP_REFERER'] . ">" . $buttonText . "</a>";
}
