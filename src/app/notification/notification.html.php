<?php foreach ($this->allMessages as $key => $messages) : ?>
	<?php if (count($messages) > 0) : ?>
		<input id="notification-toggle-<?= $key ?>" class="notification-toggle" type="checkbox" autocomplete="off" />
		<div class="notification notification-<?= $key ?>">
			<label for="notification-toggle-<?= $key ?>" class="notification-toggle-close-button noselect">&#10005;</label>
			<?php foreach ($messages as $message) : ?>
				<div>
					<?= $message ?>
				</div>
			<?php endforeach; ?>
		</div>
	<?php endif; ?>
<?php endforeach; ?>