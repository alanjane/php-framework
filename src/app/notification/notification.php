<?php

class Notification
{
	public const SUCCESS = "success";
	public const INFO = "info";
	public const ERROR = "error";

	private $allMessages = [];
	private $successMessages = [];
	private $infoMessages = [];
	private $errorMessages = [];

	public function __construct()
	{
		$this->allMessages[$this::SUCCESS] = $this->successMessages;
		$this->allMessages[$this::INFO] = $this->infoMessages;
		$this->allMessages[$this::ERROR] = $this->errorMessages;
	}

	public function addMessage($message, $type)
	{
		switch ($type) {
			case $this::SUCCESS:
				array_push($this->allMessages[$this::SUCCESS], $message);
				break;
			case $this::INFO:
				array_push($this->allMessages[$this::INFO], $message);
				break;
			case $this::ERROR:
				array_push($this->allMessages[$this::ERROR], $message);
				break;
		}
		return $this;
	}

	public function display()
	{
		require_once(APP . "/notification/notification.html.php");
	}
}
