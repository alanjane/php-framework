<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= $title ?></title>
	<link href="/css/thatsok.css" rel="stylesheet">
	<link href="/css/utils.css" rel="stylesheet">
	<link href="<?= APP ?>/notification/notification.css" rel="stylesheet">
</head>

<body>
	<?php require_once(ROOT . "/view/html/common/navigation.php"); ?>
	<section>
		<?= isset($m) ? $m->notifications->display() : "" ?>