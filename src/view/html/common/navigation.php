<?php

$loggedIn = $authActive && $AUTH->isLoggedin();

$mainMenuLinks = [
    [
        "link" => "home",
        "text" => "Home",
        "pages" => ["/", "home"],
        "condition" => ($authActive && $loggedIn) || !$authActive
    ],
    [
        "link" => "admin/home",
        "text" => "Admin",
        "pages" => ["admin/home", "admin/users"],
        "condition" => ($authActive && $loggedIn && $roles->hasRole(roles::admin))
    ],
    [
        "link" => "logout",
        "text" => "Logout" . ($USER != null ? " (" . $USER->username . ")" : ""),
        "pages" => ["logout"],
        "condition" => ($authActive && $loggedIn),
        "class" => "r"
    ],
    [
        "link" => "login",
        "text" => "Login",
        "pages" => ["auth/login"],
        "condition" => ($authActive && !$loggedIn)
    ],
    [
        "link" => "register",
        "text" => "Register",
        "pages" => ["auth/register"],
        "condition" => ($authActive && !$loggedIn)
    ]
];

// Admin submenu.
$adminSubmenuLinks = [
    [
        "link" => "admin/home",
        "text" => "Admin home",
        "pages" => ["admin/home"],
        "condition" => ($authActive && $USER !== null && $loggedIn && $roles->hasRole(roles::admin))
    ],
    [
        "link" => "admin/users",
        "text" => "Users",
        "pages" => ["admin/users", "admin/edit_user"],
        "condition" => ($authActive && $USER !== null && $loggedIn && $roles->hasRole(roles::admin))
    ]
];
$adminSubmenuPages = [];
foreach ($adminSubmenuLinks as $link) {
    foreach ($link["pages"] as $pageLink) {
        array_push($adminSubmenuPages, $pageLink);
    }
}
$displayAdminSubmenu = ($authActive && $USER !== null && $roles->hasRole(roles::admin) && in_array($view, $adminSubmenuPages));

?>

<!-- Main menu. -->
<nav>
    <span><label for="menu-toggle">MENU</label></span>
    <input type="checkbox" id="menu-toggle" autocomplete="off" />
    <div class="<?= $authActive && !$loggedIn ? 'c' : '' ?>">
        <?php foreach ($mainMenuLinks as $link) : ?>
            <?php if ($link["condition"]) : ?>
                <a href="/<?= $link["link"] ?>" class="<?= (in_array($view, $link["pages"])) ? 'a' : '' ?> <?= array_key_exists("class", $link) ? $link["class"] : "" ?>">
                    <?= $link["text"] ?>
                </a>
            <?php endif; ?>
        <?php endforeach; ?>
    </div>
</nav>

<!-- Admin submenu. -->
<?php if ($displayAdminSubmenu) : ?>
    <nav>
        <span><label for="submenu-toggle">Admin menu</label></span>
        <input type="checkbox" id="submenu-toggle" autocomplete="off" />
        <div>
            <?php foreach ($adminSubmenuLinks as $link) : ?>
                <?php if ($link["condition"]) : ?>
                    <a href="/<?= $link["link"] ?>" class="<?= (in_array($view, $link["pages"])) ? 'a' : '' ?> <?= array_key_exists("class", $link) ? $link["class"] : "" ?>">
                        <?= $link["text"] ?>
                    </a>
                <?php endif; ?>
            <?php endforeach; ?>
        </div>
    </nav>
<?php endif; ?>