<?php

$users = $AUTH->getUsers();

?>

<!-- TODO: Add user management (such as edit, remove user and modify roles for the user). There should always be at least 1 "admin" user. -->

<h1>Users</h1>

<table class="v">
    <thead>
        <tr>
            <th>Username</th>
            <th>Roles</th>
            <th>Actions</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) : ?>
            <tr>
                <td>
                    <?= $user["username"] ?>
                </td>
                <td>
                    <?php foreach ($user["roles"] as $role) : ?>
                        <?= $role["name"] ?>
                    <?php endforeach; ?>
                </td>
                <td>
                    <a href="/admin/edit_user?userId=<?= $user->id ?>">Edit</a>
                    <a href="/admin/delete_user?userId=<?= $user->id ?>">Delete</a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>