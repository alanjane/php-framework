<?php

define("ROOT", "./");
define("APP", ROOT . "/app");
define("VIEW", ROOT . "/view");
define("CONTROLLER", ROOT . "/controller");
define("G", $_GET);
define("P", $_POST);

date_default_timezone_set("America/New_York");

// Include config_env.php
if (file_exists(ROOT . "/config_env.php")) require_once(ROOT . "/config_env.php");
else {
	echo "Please configure your environment.";
	die();
}

// Default title.
$title = "Default title";

// Set isGet and isPost variables.
$isGet = true;
$isPost = false;
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$isGet = false;
	$isPost = true;
}

// Back button.
include_once(APP . "/back_button/back_button.php");

// Global notifications.
include_once(APP . "/notification/notification.php");
$notifications = new Notification();

// Logger.
include_once(APP . "/logger/logger.php");
$log = new log(ROOT . "logs/");

// Toggle authentication.
$authActive = true;

// Roles config.
require_once(APP . "/roles/roles.php");
$roles = new roles($authActive);

// Auth config.
require_once(APP . "/auth/auth.php");
$AUTH = new Auth($authActive, $isPost, $notifications, $log, $roles->roles);
