<?php

// By default all pages need auth.
$needAuth = true;
$adminPage = false;

if (
    preg_match('/^\/(\?.*)?$/', $_SERVER['REQUEST_URI']) ||
    preg_match('/^\/home(\?.*)?$/', $_SERVER['REQUEST_URI'])
) {
    $view = "home";
}
// Auth.
else if (preg_match('/^\/register(\/.*)?(\?.*)?$/', $_SERVER['REQUEST_URI'])) {
    $view = "auth/register";
    $needAuth = false;
} else if (preg_match('/^\/login(\/.*)?(\?.*)?$/', $_SERVER['REQUEST_URI'])) {
    $view = "auth/login";
    $needAuth = false;
} else if (preg_match('/^\/logout(\/.*)?(\?.*)?$/', $_SERVER['REQUEST_URI'])) {
    $view = "auth/logout";
}
// Admin.
else if (preg_match('/^\/admin\/users(\/.*)?(\?.*)?$/', $_SERVER['REQUEST_URI'])) {
    $view = "admin/users";
    $adminPage = true;
} else if (preg_match('/^\/admin\/home(\/.*)?(\?.*)?$/', $_SERVER['REQUEST_URI'])) {
    $view = "admin/home";
    $adminPage = true;
} else if (preg_match('/^\/admin\/edit_user(\/.*)?(\?.*)?$/', $_SERVER['REQUEST_URI'])) {
    $view = "admin/edit_user";
    $adminPage = true;
} else if (preg_match('/^\/admin\/delete_user(\/.*)?(\?.*)?$/', $_SERVER['REQUEST_URI'])) {
    $view = "admin/delete_user";
    $adminPage = true;
}
// 404.
else $view = "common/404";

$USER = null;
if ($needAuth && $authActive) {
    $USER = $AUTH->auth();
    $roles->setUser($USER);
}

// If non-admin trying to access admin stuff - give them 404.
if ($adminPage && !$roles->hasRole(roles::admin)) {
    $view = "common/404";
}
